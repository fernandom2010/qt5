#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    DatabaseConnect();
    DatabaseInit();
    DatabasePopulate();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::DatabaseConnect()
{
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        QDir databasePath;
        QString path = databasePath.currentPath()+"nomina.db";
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);
        db.setDatabaseName(path);
        if(!db.open())
            qWarning() << "Error al conectarse a la base de datos" << db.lastError().text();
    }
    else {
        qWarning() << "Error no existe el driver" << DRIVER;
    }
}

void MainWindow::DatabaseInit()
{
    QSqlQuery query("CREATE TABLE nomina (id INTEGER PRIMARY KEY, nombre TEXT)");

    if(!query.isActive())
        qWarning() << "Error de base de datos " << query.lastError().text();
}

void MainWindow::DatabasePopulate()
{
    QSqlQuery query;

    if(!query.exec("INSERT INTO nomina VALUES(1,'Juan Perez')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(2,'Carlos Lopez')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(3,'Miguel Sanchez')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(4,'Andres Salaz')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(5,'Mario Aguirre')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(6,'Luis Miranda')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
}


void MainWindow::on_pushButton_clicked()
{
    QSqlQuery query;
    query.prepare("SELECT nombre FROM nomina WHERE id = ?");
    query.addBindValue(ui->lineEdit->text().toInt());

    if(!query.exec())
        qWarning() << "BDD error "<< query.lastError().text();

    if(query.first())
        ui->label->setText(query.value(0).toString());
    else {
        ui->label->setText("persona no encontrada");
    }
}
