#include "widget.h"
#include "ui_widget.h"

#include <QDebug>
#include <QDir>

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    DatabaseConnect();
    DatabaseInit();
    DatabasePopulate();
    TableView();

}

Widget::~Widget()
{
    delete ui;
}

void Widget::DatabaseConnect()
{
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        QDir databasePath;
        QString path = databasePath.currentPath()+"nomina.db";
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);
        db.setDatabaseName(path);
        if(!db.open())
            qWarning() << "Error al conectarse a la base de datos" << db.lastError().text();
    }
    else {
        qWarning() << "Error no existe el driver" << DRIVER;
    }
}

void Widget::DatabaseInit()
{
    QSqlQuery query("CREATE TABLE nomina (id INTEGER PRIMARY KEY, nombre TEXT)");

    if(!query.isActive())
        qWarning() << "Error de base de datos " << query.lastError().text();
}

void Widget::DatabasePopulate()
{
    QSqlQuery query;

    if(!query.exec("INSERT INTO nomina VALUES(1,'Juan Perez')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(2,'Carlos Lopez')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(3,'Miguel Sanchez')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(4,'Andres Salaz')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(5,'Mario Aguirre')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
    if(!query.exec("INSERT INTO nomina VALUES(6,'Luis Miranda')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: "<< query.lastError().text();
}

void Widget::TableView()
{
    model = new QSqlTableModel(this);
    model->setTable("nomina");
    model->select();

    qDebug() << model->lastError().text();
    ui->tableView->setModel(model);
}

